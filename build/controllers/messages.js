"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addMessage = exports.getMessages = void 0;
const models_1 = __importDefault(require("../models"));
const normalizr_1 = require("normalizr");
function getMessages() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const aux = { id: "mensajes" };
            aux['mensajes'] = yield models_1.default.Message.find({}).lean();
            const authorSchema = new normalizr_1.schema.Entity("authors");
            const messageSchema = new normalizr_1.schema.Entity("message", { author: authorSchema }, { idAttribute: "_id" });
            const messagesSchema = new normalizr_1.schema.Entity("messages", {
                mensajes: [messageSchema]
            });
            const normalizeData = normalizr_1.normalize(aux, messagesSchema);
            return normalizeData;
        }
        catch (err) {
            throw err;
        }
    });
}
exports.getMessages = getMessages;
function addMessage(author, message, fullDate) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newMessage = {
                author,
                message,
                hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`,
                date: fullDate.toString().split(' ').slice(0, 4).join(" ")
            };
            yield models_1.default.Message.create(newMessage);
            return newMessage;
        }
        catch (err) {
            throw err;
        }
    });
}
exports.addMessage = addMessage;

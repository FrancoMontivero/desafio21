export default function message(mongoose: any) {
    const schema = new mongoose.Schema({
        author: {
            id: {type: String, require: true},
            name: {type: String, require: true},
            lastname: {type: String, require: true},
            age: {type: Number, require: true},
            alias: {type: String, require: true},
            avatar: {type: String, require: true}
        },
        message: {type: String, require: true},
        hour: {type: String, require: true},
        date: {type: String, require: true}
    })

    const model = mongoose.model("message", schema)

    return {name: "Message", model};
}
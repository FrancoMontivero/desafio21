import { emitProducts } from './socket';
import db from '../models';
import faker from 'faker';


export interface ProductInterface {
    id?: number,
    title: string,
    price: number,
    thumbnail: string
}

export async function getProducts(): Promise<Array<ProductInterface>> { 
    const Products = await db.Product.find();
    return Products;
};

export async function getProduct(id: number | string): Promise<Object | ProductInterface> {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const product: ProductInterface = db.Product.find({id}).exec();
    if(product) return product
    else throw new Error("Producto no encontrado");
}

export async function addProduct(title: string, price: string | number, thumbnail: string): Promise<ProductInterface> {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 

    const newProduct = { title, price, thumbnail };
    await db.Product.insertMany([newProduct])
    try { 
        const Products = await db.Product.find();
        emitProducts(Products); 
    }
    finally { return newProduct }
}

export async function updateProduct(id: number | string, title: string, price: number, thumbnail: string): Promise<ProductInterface> {
    if(!(title && price && thumbnail)) throw new Error("Hay parametros vacios o indefinidos");
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico");
    else if(typeof id !== "number") id = parseInt(id);
    if(Number.isNaN(Number(price))) throw new Error("El precio no puede contener caracteres no numericos")
    else if(typeof price !== "number") price = parseFloat(price); 
    const product = await db.Product.find({id}).exec();
    if(!product) throw new Error(`No existe un producto con el id ${id}`);
    const newProduct = { title, price, thumbnail };
    try { 
        await db.Product.updateSyncProduct({id}, newProduct);
        const Products = await db.Product.find();
        emitProducts(Products); 
    }
    catch(err) { throw err; }
    finally {
        return product; 
    }
}

export async function removeProduct(id: number | string) {
    if(Number.isNaN(Number(id))) throw new Error("El id del producto debe ser un caracter numerico")
    else if(typeof id !== "number") id = parseInt(id);
    const product = await db.Product.find({id}).exec();
    if(!product) throw new Error(`No existe un producto con el id ${id}`);
    try { 
        await db.Product.deleteOne({id});
        const Products = await db.Product.find({id}).exec();
        emitProducts(Products); 
    }
    finally { return product; }
}

export function getProductsFaker(amount: number) {
    if(Number.isNaN(Number(amount))) throw new Error("La cantiadad de productos debe ser un caracter numerico");
    else if(typeof amount !== "number") amount = parseInt(amount);
    let products = [];
    for(let i = 0 ; i < amount ; i++) { 
        const newProduct = { 
            "titulo": faker.commerce.productName(),
            "precio": faker.commerce.price(),
            "foto": faker.image.imageUrl()
        }
        products.push(newProduct);
    }
    return products;
}

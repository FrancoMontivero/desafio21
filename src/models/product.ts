export default function products(mongoose: any) {
    const schema = new mongoose.Schema({
        id: {type: Number},
        title: {type: String, require: true},
        price: {type: Number, require: true},
        thumbnail: {type: String, require: true}
    })

    const model = mongoose.model("product", schema)

    return {name: "Product", model};
}
const { normalize, schema, denormalize } = require('normalizr');

const myData = [
  {
    author: {
      id: 'pepe@hotmail.com',
      name: 'Pepe',
      lastname: 'asdasd',
      age: 123,
      alias: 'asdasd123',
      avatar: 'asdasdas'
    },
    _id: "60a759e6f3c90374660f8b64",
    message: 'hola',
    hour: '03:57',
    date: 'Fri May 21 2021',
    __v: 0
  },
  {
    author: {
      id: 'sad@asd.coim',
      name: 'dasd',
      lastname: 'dsad',
      age: 2,
      alias: 'dqsd213',
      avatar: 'dasdas'
    },
    _id: "60a7b99b40788c0d7039ea6a",
    message: 'asdas',
    hour: '10:46',
    date: 'Fri May 21 2021',
    __v: 0
  },
  {
    author: {
      id: 'asdasd123@sda.com',
      name: 'sdf',
      lastname: 'qfsdkj',
      age: 324,
      alias: 'fdslkfjwelk;j423',
      avatar: "fsdkjfl'kws"
    },
    _id: "60a7ba5ebc0b660e9e541b31",
    message: 'sadas',
    hour: '10:49',
    date: 'Fri May 21 2021',
    __v: 0
  }
]

const aux = { id: "mensajes", "mensajes": myData };

const authorSchema = new schema.Entity("authors");
const messageSchema = new schema.Entity(
    "message", 
    { author: authorSchema }, 
    { idAttribute: "_id" }
);
const messagesSchema = new schema.Entity("messages", {
    mensajes: [messageSchema]
});
const normalizeData = normalize(aux, messagesSchema);

console.log(normalizeData);
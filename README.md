# Desafío 21 - JSON Normalizacion
_desafío 20 de Franco Montivero_

## ⚙️ Instalación y pruebas
Primero instalar todos las dependencias del proyecto
```
npm install
```
### Variables de entorno
Para poder hacer pruebas en el ambiente de desarrollo, se puede crear un archivo development.env que sirve para poder definir variables de entorno, el archivo developmentExample.env contiene un ejemplo de las variables de entorno que puedes usar.  
  
Los valores por defecto para el ambiente de desarrollo son los siguientes:
```
PORT=8080
ORIGIN_CLIENT='http://localhost:8081'
SHOW_REQUEST='enabled'
HOST_MONGO='mongodb://localhost:27017'
```
Si se requiere usar algunas de estas variables de entorno cuando se ejecutan los test, crear un archivo test.env y agregar las variables deseadas para que se ejecuten en los test.  

### Scripts
En el archivo package.json se definen los siguientes scripts:
* **build**: transpila los archivos typescript que se encuentran en el directorio*"tsc"* al directorio *"src"*.
* **build:watcher**: transpila los archivos typescript que se encuentran en el directorio *"src"* al directorio *"build"* y se queda esperando cambios para volver a transpilar.
* **start:dev**: levanta el servidor en un ambiente de desarrollo.
* **start:dev -watcher**: levanta el servidor en un ambiente de desarrollo, quedando a la escucha de cambios y aplicando estos cambios al servidor de forma automática.

## 📜 Documentación de la API 

| Método |     Recurso   |  Descripción  |
|  ----- | ------------- | ------------- |
| GET    | /api/productos/listar  | Permite Listar todos los productos cargados o devuelve un error en caso de no haber productos.  |
| GET    | /api/productos/listar/*:id*| Si el parámetro *id* es un carácter numérico y existe un producto con ese id, devuelve el producto|
| GET    | /api/productos/vista-test?*amount:id*| Si el query params *amount* es un carácter numérico           devuelve amount cantidad de productos generados por faker|
| POST   | /api/productos/guardar | Permite cargar un producto.  |
| PUT    | /api/productos/actualizar/*:id* | Si el parámetro *id* es un carácter numérico y existe un producto con ese id, permite actualizar la información del producto con ese id.|
| DELETE | /api/productos/borrar/*:id*  | Si el parámetro *id* es un carácter numérico y existe un producto con ese id, permite borrar el producto con ese id. |

Tomando como base que se encuentren cargados los siguientes productos:
```
[
    {
        "id":1,
        "title":"Escuadra",
        "price":123.45,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
    },
    {
        "id":2,
        "title":"Calculadora",
        "price":234.56,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/calculator-math-tool-school-256.png"
    },
    {
        "id":3,
        "title":"Globo Terráqueo",
        "price":345.67,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/globe-earth-geograhy-planet-school-256.png"
    }
]
```
**Se describen los siguientes endpoints:**

### Obtener los productos
Esta llamada permite obtener todos los productos cargados.  

Llamada:
```
curl -X GET http://localhost:$PORT/api/productos/listar
```
Ejemplo:
```
curl -X GET http://localhost:8080/api/productos/listar
```
Respuesta:
```
[
    {
        "id":1,
        "title":"Escuadra",
        "price":123.45,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
    },
    {
        "id":2,
        "title":"Calculadora",
        "price":234.56,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/calculator-math-tool-school-256.png"
    },
    {
        "id":3,
        "title":"Globo Terráqueo",
        "price":345.67,
        "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/globe-earth-geograhy-planet-school-256.png"
    }
]
```
Respuesta en caso de que no haya productos cargados:
```
{
    "error": "No hay productos cargados"
}
```

### Obtener un producto por su id
Esta llamada permite obtener un producto por su id.  

Llamada:
```
curl -X GET http://localhost:$PORT/api/productos/listar/$ID
```
Ejemplo:
```
curl -X GET http://localhost:8080/api/productos/listar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "Producto no encontrado"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del producto debe ser un caracter numerico"
}
```

### Cargar un producto
Esta llamada permite cargar un producto nuevo.  

Llamada:
```
curl -X POST -H "Content-Type: application/json" -d '{
    "title": "Some title",
    "price": 1111,
    "thumbnail": "Some url"
}'
http://localhost:$PORT/api/productos/guardar
```
Ejemplo:
```
curl -X POST -H "Content-Type: application/json" -d '{
    "title": "Escuadra",
    "price": 123.45,
    "thumbnail": "https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}'
http://localhost:8080/api/productos/guardar
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que falten parametros:
```
{
    "error": "Hay parametros vacios o indefinidos"
}
```
Respuesta en caso de que el precio no sea un numero:
```
{
    "error": "El precio no puede contener caracteres no numericos"
}
```

### Actualizar un producto
Esta llamada permite actualizar con su id, un producto existente.  

Llamada:
```
curl -X PUT -H "Content-Type: application/json" -d '{
    "title": "New title",
    "price": 2222,
    "thumbnail": "New url"
}'
http://localhost:$PORT/api/productos/actualizar/$ID
```
Ejemplo:
```
curl -X PUT -H "Content-Type: application/json" -d '{
    "title": "Nueva escuadra",
    "price": 123.45,
    "thumbnail": "https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}'
http://localhost:8080/api/productos/actualizar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Nueva escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del producto debe ser un caracter numerico"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "No existe un producto con el id 1"
}
```
Respuesta en caso de que falten parametros:
```
{
    "error": "Hay parametros vacios o indefinidos"
}
```
Respuesta en caso de que el precio no sea un numero:
```
{
    "error": "El precio no puede contener caracteres no numericos"
}
```

### Borrar un producto
Esta llamada permite borrar con su id, un producto.  

Llamada:
```
curl -X DELETE http://localhost:$PORT/api/productos/borrar/$ID
```
Ejemplo: 
```
curl -X DELETE http://localhost:8080/api/productos/borrar/1
```
Respuesta:
```
{
    "id":1,
    "title":"Escuadra",
    "price":123.45,
    "thumbnail":"https://cdn3.iconfinder.com/data/icons/education-209/64/ruler-triangle-stationary-school-256.png"
}
```
Respuesta en caso de que el id no sea un caracter numerico:
```
{
    "error": "El id del producto debe ser un caracter numerico"
}
```
Respuesta en caso de que no haya productos un producto cargado con ese id:
```
{
    "error": "No existe un producto con el id 1"
}
```

### Formulario de productos  
Esta llamada permite obtener un archivo estatico .html que contiene un formulario para poder cargar productos y ademas mostrar en una tabla los cambios que se realizan en los productos.

Llamada:
```
curl -X GET http://localhost:$PORT/static/products.html
```
Ejemplo:
```
curl -X GET http://localhost:8080/static/products.html
```
Respuesta:  
![Vista con productos cargados](./img/form-products.png)     

Respuesta en caso de que no haya productos cargados:  
![Vista sin productos cargados](./img/form-products-empty.png)  

### Chat
Esta llamada permite acceder a un chat publico.
Llamada:
```
curl -X GET http://localhost:$PORT/static/chat.html
```
Ejemplo:
```
curl -X GET http://localhost:8080/static/chat.html
```

![Formulario de email para chatear](./img/chat-form-email.jpeg)
![Chat vacio](./img/chat-empty.jpeg)
![Chat con mensajes](./img/chat-messages.jpeg)
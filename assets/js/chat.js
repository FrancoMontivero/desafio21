var maxScrollTop;
var scrollHeight;
var email;
var name;
var lastname;
var age;
var alias;
var avatar;

function onSubmit(e) {
    e.preventDefault();
    const message = e.target.children[0].value;
    e.target.children[0].value = "";
    const dataMessage = {date: Date.now(), message, author: { id: email, name, lastname, age, alias, avatar }};
    io().emit("new-message", dataMessage);
};

function createMessage(message) {
    return  '<li class="message">' +
                '<div class="message__from-container">' +
                    '<span class="message__from">' + message.from + '</span>' +
                '</div>' +
                '<div class="message__text-container">' +
                    '<span class="message__text">' + message.message + '</span>' +
                '</div>' +
                '<div class="message__hour-container">' +
                    '<span class="message__hour">' + message.hour + '</span>' +
                '</div>' +
            '</li>'
}

function createListMessage(messages) {
    let aux = ''
    for(date in messages) {
        let msg = '<div class="messages__date">' +
                    '<span>' + date + '</span>' +
                  '</div>';
        for(message of messages[date]) {
            msg += createMessage(message);
        }
        aux += '<ol class="messages-container">' + msg + '</ol>';
    }
    return aux;
}

function initMessages(messages) {
    const messageContainerSelector = document.querySelector(".messages");
    if(Object.keys(messages).length === 0) {
        messageContainerSelector.innerHTML = '<div class="w-100 h-100 d-flex align-items-center justify-content-center">' + 
                                                '<h2 class="text-center text-white" id="alert-chat-empty">' +
                                                    'El chat esta vacio' + 
                                                '</h2>' +
                                            '</div>'
    } else {
        messageContainerSelector.innerHTML = createListMessage(messages);
    }
    messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
    maxScrollTop = messageContainerSelector.scrollTop;
    scrollHeight = messageContainerSelector.scrollHeight;
}

function addMessage(message) {
    const messageContainerSelector = document.querySelector(".messages");
    const date = Object.keys(message)[0];
    let isUploadedDate = false;
    let i = 0;
    if(!document.querySelector("#alert-chat-empty")){
        while(!isUploadedDate && i < messageContainerSelector.children.length) {
            if(messageContainerSelector.children[i].children[0].children[0].innerText === date) {
                messageContainerSelector.children[i].innerHTML += createMessage(message[date]);
                if(maxScrollTop <= 300 || messageContainerSelector.scrollTop >= maxScrollTop - 300) {
                    messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
                }
                maxScrollTop += messageContainerSelector.scrollHeight - scrollHeight;
                scrollHeight = messageContainerSelector.scrollHeight;
                isUploadedDate = true;
            }
            i++;
        }
    }else messageContainerSelector.innerHTML = "";
    if(!isUploadedDate) {
        messageContainerSelector.innerHTML += '<ol class="messages-container">' +
                                                '<div class="messages__date">' +
                                                    '<span>' + date + '</span>' +
                                                    '</div>' + 
                                                    createMessage(message[date]) + 
                                              '</ol>'
    }
}

function configureScroll() {
    const buttonScrollBottomSelector = document.querySelector("#btn-scroll-bottom");
    const messageContainerSelector = document.querySelector(".messages");
    let prevScroll = messageContainerSelector.scrollTop
    messageContainerSelector.onscroll = e => {
        if(e.target.scrollTop > prevScroll && e.target.scrollTop <= maxScrollTop - 200) {
            buttonScrollBottomSelector.style.display = "block";
        } else buttonScrollBottomSelector.style.display = "none";
        prevScroll = messageContainerSelector.scrollTop;
    };
}

function configureButtonScrollBottom() {
    const messageContainerSelector = document.querySelector(".messages");
    document.querySelector("#btn-scroll-bottom").onclick = e => {
        messageContainerSelector.scrollTop = messageContainerSelector.scrollHeight;
    }
}

function activeChat() {
    document.querySelector(".chat__input").onsubmit = onSubmit;

    const authorSchema = new normalizr.schema.Entity("authors");

    const messageSchema = new normalizr.schema.Entity(
        "message", 
        { author: authorSchema }, 
        { idAttribute: "_id" }
    );

    const messagesSchema = new normalizr.schema.Entity("messages", {
        mensajes: [messageSchema]
    });

    io().on("messages", normalizeMessage => {
        console.log(normalizeMessage);
        console.log(JSON.stringify(normalizeMessage).length);
        const desnormalizeMessage = normalizr.denormalize(normalizeMessage.result, messagesSchema,normalizeMessage.entities)
        let messages = {};
        for(const message of desnormalizeMessage.mensajes) {
            if(messages.hasOwnProperty(message.date)) {
                messages[message.date].push({
                    from: message.author.name + " " + message.author.lastname,
                    message: message.message, 
                    hour: message.hour});
            } else {
                messages[message.date] = [{
                    from: message.author.name + " " + message.author.lastname,
                    message: message.message, 
                    hour: message.hour}];
            }
        }
        console.log(messages);
        console.log(JSON.stringify(messages).length);
        initMessages(messages);
    });
    
    io().on("new-message", message => {
        const newMessage = {
            [message.date]: {
                from: message.author.name + " " + message.author.lastname,
                message: message.message,
                hour: message.hour
            }
        }
        addMessage(newMessage);
    });

    configureScroll();
    configureButtonScrollBottom();
}

function main() {
    const formEmailSelector = document.querySelector(".form-email");
    formEmailSelector.addEventListener('submit', function(e) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        email = formData.get('email');
        name = formData.get('name');
        lastname = formData.get('lastname');
        age = formData.get('age');
        alias = formData.get('alias');
        avatar = formData.get('avatar');
        e.currentTarget.remove();
        activeChat();
    })
}

main();
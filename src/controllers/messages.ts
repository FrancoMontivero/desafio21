import db from '../models';
import { normalize, schema } from 'normalizr';
import { inspect } from 'util';
import message from '../models/message';


export interface Author {
    id: string,
    name: string,
    lastname: string,
    age: number,
    alias: string,
    avatar: string
}

export interface MessageInterface {
    author: Author,
    message: string,
    hour: string,
    date: string
}

export interface NormalizeMessages {
    entities: {
        authors: {[key: string]: Author},
        message: {[key: string]: {
            author: string,
            _id: string,
            message: string,
            hour: string,
            date: string    
        }}
    }, result: "mensajes"
}

export async function getMessages(): Promise<NormalizeMessages> {
    try {
        const aux: { [key: string]: any } = { id: "mensajes" };
        aux['mensajes'] = await db.Message.find({}).lean();

        const authorSchema = new schema.Entity("authors");

        const messageSchema = new schema.Entity(
            "message", 
            { author: authorSchema }, 
            { idAttribute: "_id" }
        );

        const messagesSchema = new schema.Entity("messages", {
            mensajes: [messageSchema]
        });

        const normalizeData: NormalizeMessages = normalize(aux, messagesSchema);

        return normalizeData;
    } catch (err) {
        throw err;
    }
}

export async function addMessage(author: Author, message: string, fullDate: Date): Promise<MessageInterface> {
    try {
        const newMessage: MessageInterface = {
            author,
            message, 
            hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`,
            date: fullDate.toString().split(' ').slice(0, 4).join(" ")
        };
        await db.Message.create(newMessage);
        return newMessage;
    } catch (err) {
        throw err;
    }
}
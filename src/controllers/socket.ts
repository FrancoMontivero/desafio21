import { getProducts, ProductInterface } from './products';
import { getMessages, addMessage, Author, MessageInterface } from './messages';
import { Server, Socket } from 'socket.io';

let refIo: Server;

export function initServer(io: Server): void {
    refIo = io;
    io.on("connection", async (socket: Socket) => {
        emitProducts(await getProducts());
        emitMessages(await getMessages());

        socket.on("new-message", async (data: {author: Author, message: string, date: string}) => {
            const newMessage = await addMessage(data.author, data.message, new Date(data.date));
            if(newMessage) emitNewMessage(newMessage);
        });
    });
}

export function emitProducts(products: Array<ProductInterface>): void {
    refIo.emit("products", products);
};

export async function emitMessages(messages: Object): Promise<void> {
    refIo.emit("messages", messages);
}

export async function emitNewMessage(message: MessageInterface): Promise<void> {
    refIo.emit("new-message", message);
}